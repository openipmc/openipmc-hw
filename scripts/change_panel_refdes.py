from pcbnew import *

board = GetBoard()
for module in board.GetModules():
  ref = module.GetReference()
  x = module.GetPosition()[0]
  y = module.GetPosition()[1]
  
  # 150 mm in x and 60 mm in y
  x_boundary = 150000000
  y_boundary =  60000000
    
  if (ref[0] != "F" and ref[1] != "D"):
    if x > x_boundary and y > y_boundary: # dimm 0
      module.SetReference(ref + "_0")
    elif x < x_boundary and y > y_boundary: # dimm 1
      module.SetReference(ref + "_1") 
    elif x < x_boundary and y < y_boundary: # dimm 2
      module.SetReference(ref + "_2")  
    elif x > x_boundary and y < y_boundary: # dimm 3
      module.SetReference(ref + "_3") 
