# Execute script to rename refdes according to position in the panel

1. open python scripting from kicad
2. `cd /Data/KICAD/KiCad_PCBs/openipmc-hw/scripts`
3. `exec(open("/Data/KICAD/KiCad_PCBs/openipmc-hw/scripts/change_panel_refdes.py"))`
4. at the end, the table showing the board stackup will have a _1, just remove it. (ideally this could be added to the script, but no time now)


# Execute script to rename footprint names for Pick and Place at KIT

1. `cd /Data/KICAD/KiCad_PCBs/openipmc-hw/bom`
2. `./change_footprint.sh openipmc-hw_panel-bottom-pos.csv`
2. `./change_footprint.sh openipmc-hw_panel-top-pos.csv`
