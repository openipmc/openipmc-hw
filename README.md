# OpenIPMC-HW

![OpenIPMC-HW](/images/openipmc-logo-HW-v2.png)

## 3D Views

### Top

![Top View 3D](/images/v1.2/Top_3D.png)

### Bottom

![Bottom View 3D](/images/v1.2/Bottom_3D.png)


## Panelization process

1. Make sure you have the last state of the `openipmc-hw.kicad_pcb` file in git
2. Toggle single layer mode, select and remove everything in the `Edge.Cuts` layer and save
3. Open the Template Board `openipmc-hw_template.kicad_pcb` in a standalone pcbnew instance by clickin the pcb file, not the project file 
4. File -> Apend Board... and Select the original design `openipmc-hw.kicad_pcb` 
5. Adjust the grid so that it is 1mm, then click on the GUI when the pasted items are located at (X 16.0 Y 155.2). This should put the DIMM on the first slot of the template
6. With the Board still selected, allow for Locked items to be selected, then using Control+Shift deselect the notes and the table, then create an array by pressing Control+T (having only the DIMM selected)
7. Select parameters 1x5 with 0 in X and 20.3 in Y spacing
![Board Array](images/v1.2/panel_create_array.png)
8. press b to re-fill zones and make them aware of the mouse-bites
9. Save as the Panel Board `openipmc-hw_panel.kicad_pcb`
![Panel 3D](images/v1.2/Panel_Top_3D.png)
10. Revert original design in git to recover `Edge.Cuts` layer
11. git commit Panel Board (the Template should have no changes)

# Licensing
OpenIPMC-HW is Copyright 2020-2023 of Luis Ardila-Perez, Luigi Calligaris, Andre Cascadan. OpenIPMC-HW is released under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/). Please refer to the LICENSE document included in this repository.
